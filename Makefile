
UNIX=1
#OSX=1

CFLAGS=-Wall -g

ifdef OSX
SHARED=-dynamiclib
SO=dylib
CFLAGS += -DLD_PRELOAD=\"DYLD_INSERT_LIBRARIES\" -DTANLIB=\"tanlib.dylib\"
endif

ifdef UNIX
SHARED=-ldl -shared 
SO=so
CFLAGS+=-fPIC
endif

all: tan tanlib.$(SO)

tan: tan.o
	$(CC) $(LDFLAGS) -o $@ $^

tanlib.$(SO): tanlib.o utils.o hash.o
	$(CC) $(LDFLAGS) $(SHARED) -o $@ $^

clean:
	rm -f *.$(SO) *.o tan

