#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "hash.h"

struct hashitem
{
	const void *key;
	const void *value;
	struct hashitem *next;
};
	
struct hash
{
	int hashsize;
	unsigned int (*hashfunc)(struct hash *, const void *);
	int (*comparefunc)(struct hash *, const void *, const void *);
	int (*deletefunc)(struct hash *, const void *, const void *);
	struct hashitem **array;
};

static struct hash *create_hash(int hashsize, unsigned int (*hashfunc)(struct hash *, const void *), int (*comparefunc)(struct hash *, const void *, const void *),int (*deletefunc)(struct hash *, const void *, const void *))
{
	struct hash *h = (struct hash *)malloc(sizeof(struct hash));
	if(!h)
		return NULL;

	h->hashsize = hashsize;
	h->hashfunc = hashfunc;
	h->comparefunc = comparefunc;
	h->deletefunc = deletefunc;
	h->array = (struct hashitem **)malloc(sizeof(struct hashitem *)*hashsize);
	if(!h->array)
	{
		free(h);
		return NULL;
	}

	memset(h->array,0,sizeof(struct hashitem *)*hashsize);
	return h;
}

static int hash_add(struct hash *hash, const void *key, const void *value)
{
	int item = hash->hashfunc(hash,key);
	struct hashitem *newitem = (struct hashitem *)malloc(sizeof(struct hashitem));

	if(!newitem)
		return -1;

	newitem->next = hash->array[item];
	newitem->key = key;
	newitem->value = value;
	hash->array[item]=newitem;
	return 0;
}

static const void *hash_lookup(struct hash *hash, const void *key)
{
	struct hashitem *item = hash->array[hash->hashfunc(hash,key)];
	for(;item;item=item->next)
	{
		if(!hash->comparefunc(hash,item->key,key))
			break;
	}
	return item->value;
}

static int hash_delete(struct hash *hash, const void *key)
{
	struct hashitem *lastitem = NULL;
	struct hashitem **pitem = &hash->array[hash->hashfunc(hash,key)];
	struct hashitem *item = *pitem;
	for(;item;item=item->next)
	{
		if(!hash->comparefunc(hash,item->key,key))
			break;
		lastitem = item;
	}

	if(!item)
		return -1;
	
	if(lastitem)
		lastitem->next = item->next;
	else
		(*pitem) = item->next;

	hash->deletefunc(hash, item->key, item->value);
	free(item);
	
	return 0;
}

static unsigned int hash_int(struct hash *hash, const void *arg)
{
	return ((int)(size_t)arg) % hash->hashsize;
}

static int compare_int(struct hash *hash, const void *arg1, const void *arg2)
{
	// This doesn't properly handle signed int.  I don't think this matters.
	return ((int)(size_t)arg1) - ((int)(size_t)arg2);
}

static int delete_int(struct hash *hash, const void *key, const void *value)
{
	return 0;
}

struct hash *create_hash_int(int hashsize)
{
        return create_hash(hashsize,hash_int,compare_int,delete_int);
}

int add_hash_int(struct hash *hash, int key, const void *value)
{
        return hash_add(hash,(void*)(size_t)key,value);
}

void *lookup_hash_int(struct hash *hash, int key)
{
        return (void*)hash_lookup(hash,(void*)(size_t)key);
}

int delete_hash_int(struct hash *hash, int key)
{
        return hash_delete(hash,(void*)(size_t)key);
}

static unsigned int hash_in(struct hash *hash, const void *arg)
{
	struct in_addr *in = (struct in_addr *)arg;
	return ntohl(in->s_addr) % hash->hashsize;
}

static int compare_in(struct hash *hash, const void *arg1, const void *arg2)
{
	return ((struct in_addr *)arg1)->s_addr - ((struct in_addr *)arg2)->s_addr;
}

static int delete_in(struct hash *hash, const void *key, const void *value)
{
	free((void*)key);
	return 0;
}

struct hash *create_hash_in4(int hashsize)
{
	return create_hash(hashsize,hash_in,compare_in,delete_in);
}

int add_hash_in4(struct hash *hash, const struct in_addr *key, const void *value)
{
	return hash_add(hash,key,value);
}

void *lookup_hash_in4(struct hash *hash, const struct in_addr *key)
{
	return (void*)hash_lookup(hash,key);
}

int delete_hash_in4(struct hash *hash, const struct in_addr *key)
{
	return hash_delete(hash,key);
}

static unsigned int hash_in6(struct hash *hash, const void *arg)
{
	struct in6_addr *in6 = (struct in6_addr *)arg;
	unsigned int hashvalue = 0;
	int n;

	for(n=0; n<16; n++)
	{
      		hashvalue += in6->s6_addr[n];
		hashvalue *= 13;
	}
  	return hashvalue % hash->hashsize;
}

static int compare_in6(struct hash *hash, const void *arg1, const void *arg2)
{
	return memcmp(((struct in6_addr *)arg1)->s6_addr,((struct in6_addr *)arg2)->s6_addr, 16);
}

static int delete_in6(struct hash *hash, const void *key, const void *value)
{
	free((void*)key);
        return 0;
}

struct hash *create_hash_in6(int hashsize)
{
	return create_hash(hashsize,hash_in6,compare_in6,delete_in6);
}

int add_hash_in6(struct hash *hash, const struct in6_addr *key, const void *value)
{
        return hash_add(hash,key,value);
}

void *lookup_hash_in6(struct hash *hash, const struct in6_addr *key)
{
        return (void*)hash_lookup(hash,key);
}

int delete_hash_in6(struct hash *hash, const struct in6_addr *key)
{
        return hash_delete(hash,key);
}


