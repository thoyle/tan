#ifndef HASH__H
#define HASH__H

struct hash;

struct hash *create_hash_int(int hashsize);
int add_hash_int(struct hash *hash, int key, const void *value);
void *lookup_hash_int(struct hash *hash, int key);
int delete_hash_int(struct hash *hash, int key);

struct hash *create_hash_in4(int hashsize);
int add_hash_in4(struct hash *hash, const struct in_addr *key, const void *value);
void *lookup_hash_in4(struct hash *hash, const struct in_addr *key);
int delete_hash_in4(struct hash *hash, const struct in_addr *key);

struct hash *create_hash_in6(int hashsize);
int add_hash_in6(struct hash *hash, const struct in6_addr *key, const void *value);
void *lookup_hash_in6(struct hash *hash, const struct in6_addr *key);
int delete_hash_in6(struct hash *hash, const struct in6_addr *key);

#endif

