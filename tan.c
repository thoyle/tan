#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

#ifndef LD_PRELOAD
#define LD_PRELOAD "LD_PRELOAD"
#endif

#ifndef TANLIB
#define TANLIB "tanlib.so"
#endif

static void usage(const char *prog)
{
	fprintf(stderr,"Usage: %s [-d] [-n] [-a library] <program and arguments>\n", prog);
}

int main(int argc, char *argv[])
{
	char tanlib[PATH_MAX];
	char *p;
	char tmp[16];
	const char *prog = argv[0];
	int debug=0,nochild=0;
	char *alg=NULL;

	if(argc<2) { usage(prog); return -1; }
	
	while(argv[1][0]=='-')
	{
		switch(argv[1][1])
		{
			case 'd': debug++; break;
			case 'n': nochild++; break;
			case 'a': alg=argv[1]+1; break;
			default: usage(prog); return -1;
		}
		argv++;
		argc--;
	}	

	if(argc<2) { usage(prog); return -1; }

	if(argv[0][0]=='/')
		strcpy(tanlib, argv[0]);
	else
	{
		getcwd(tanlib,sizeof(tanlib));
		strcat(tanlib,"/");
		strcat(tanlib,argv[0]);
	}

	p=strrchr(tanlib,'/');
	if(!p) p=tanlib;
	else p++;
	strcpy(p, TANLIB);

	setenv(LD_PRELOAD,tanlib,1);
	sprintf(tmp,"%d",debug);
	setenv("__TAN_DEBUG__",tmp,1);
	sprintf(tmp,"%d",nochild);
	setenv("__TAN_NOCHILD__",tmp,1);
	if(alg)
		setenv("__TAN_ALG__",alg,1);

	execvp(argv[1], argv+1);
	fprintf(stderr, "Unable to execute %s: %s\n",argv[1],strerror(errno));
	return -1;
}

