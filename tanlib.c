#include <dlfcn.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"
#include "hash.h"

#define LIST_SIZE 1024

struct item_t
{
	int dummy;
};

#if defined(RTLD_NEXT)
#define REAL_LIBC RTLD_NEXT
#else
#define REAL_LIBC ((void *) -1L)
#endif

#ifndef min
#define min(a,b) (((a)<(b))?(a):(b))
#endif

static struct hash *fd_list; 

static struct __old_fn
{
	int (*close)(int);
	int (*dup)(int);
	int (*dup2)(int, int);
	int (*socket)(int, int, int);
	int (*connect)(int, const struct sockaddr *, socklen_t);
	int (*accept)(int,struct sockaddr *, socklen_t *);
	int (*getpeername)(int, struct sockaddr *, socklen_t *);
	int (*bind)(int, const struct sockaddr *, socklen_t);
	struct hostent *(*gethostbyname)(const char *);
	struct hostent *(*gethostbyname2)(const char *,int);
	struct hostent *(*gethostbyaddr)(const void *, socklen_t, int);
	int (*getsockopt)(int, int, int, void *, socklen_t *);
	int (*setsockopt)(int, int, int, const void *, socklen_t);
	ssize_t (*sendto)(int, const void *, size_t, int, const struct sockaddr *, socklen_t);
        ssize_t (*recvfrom)(int, void *, size_t, int, struct sockaddr *, socklen_t *);
	ssize_t (*recvmsg)(int, struct msghdr *, int);
} fn;

// This hack means the osx builds aren't thread safe..
#ifdef __APPLE__
#define __thread
#endif
	
static __thread struct hostent he = {0};
static __thread struct in_addr *he_buf = NULL;

static struct item_t *lookup_socket(int socket)
{
	return (struct item_t *)lookup_hash_int(fd_list,socket);
}

static struct item_t *add_socket(int socket)
{
	struct item_t *item = (struct item_t *)malloc(sizeof(struct item_t));
	add_hash_int(fd_list,socket,item);
	return item;
}

static void delete_socket(int socket)
{
	struct item_t *item = lookup_socket(socket);
	if(item)
	{
		delete_hash_int(fd_list,socket);
		free(item);
	}
}

static int init(void)
{
	if(fn.socket)
		return 0;

	fn.socket = (int (*)(int, int, int)) dlsym(REAL_LIBC,"socket");
	if(!fn.socket)
	{
		DEBUG(1,"Unable to init socket functions");
		return -1;
	}

	fn.close = (int (*)(int)) dlsym(REAL_LIBC,"close");
	fn.dup = (int (*)(int)) dlsym(REAL_LIBC,"dup");
	fn.dup2 = (int (*)(int,int)) dlsym(REAL_LIBC,"dup2");
	fn.connect = (int (*)(int, const struct sockaddr *, socklen_t)) dlsym(REAL_LIBC,"connect");
	fn.accept = (int (*)(int, struct sockaddr *, socklen_t *)) dlsym(REAL_LIBC,"accept");
	fn.getpeername = (int (*)(int, struct sockaddr *, socklen_t *)) dlsym(REAL_LIBC,"getpeername");
	fn.bind = (int (*)(int, const struct sockaddr *, socklen_t)) dlsym(REAL_LIBC,"bind");
	fn.gethostbyname = (struct hostent *(*)(const char *)) dlsym(REAL_LIBC,"gethostbyname");
	fn.gethostbyname2 = (struct hostent *(*)(const char *,int)) dlsym(REAL_LIBC,"gethostbyname2");
	fn.gethostbyaddr = (struct hostent *(*)(const void *, socklen_t, int)) dlsym(REAL_LIBC,"gethostbyaddr");
	fn.getsockopt = (int (*)(int, int, int, void *, socklen_t *)) dlsym(REAL_LIBC,"getsockopt");
	fn.setsockopt = (int (*)(int, int, int, const void *, socklen_t)) dlsym(REAL_LIBC,"setsockopt");
	fn.sendto = (ssize_t (*)(int, const void *, size_t, int, const struct sockaddr *, socklen_t)) dlsym(REAL_LIBC,"sendto");
        fn.recvfrom = (ssize_t (*)(int, void *, size_t, int, struct sockaddr *, socklen_t *)) dlsym(REAL_LIBC,"recvfrom");
	fn.recvmsg = (ssize_t (*)(int, struct msghdr *, int)) dlsym(REAL_LIBC,"recvmsg");

	fd_list = create_hash_int(LIST_SIZE);
	
	utils_init();

	return 0;
}

__attribute__((constructor)) static void __init(void)
{
	if(init()) return;
	DEBUG(1,"Tan is live!\n");
}

int close(int socket)
{
	struct item_t *item;

	if(init()) return -1;

	item = lookup_socket(socket);
	if(item)
	{
		DEBUG(1,"close(%d)",socket);
		delete_socket(socket);
	}
	return fn.close(socket);
}

int dup(int socket)
{
	struct item_t *item;
	int ret;
	if(init()) return -1;
	ret = fn.dup(socket);
	item = lookup_socket(socket);
	if(ret>0 && item)
	{
		DEBUG(1,"dup(%d)=%d",socket,ret);
		/* item = */add_socket(ret);
	}
	return ret;
}

int dup2(int socket, int dest)
{
	struct item_t *item;
	int ret;
	if(init()) return -1;
	ret = fn.dup2(socket, dest);
	item = lookup_socket(socket);
	if(ret>0 && item)
	{
		DEBUG(1,"dup2(%d,%d)=%d",socket,dest,ret);
		/* item = */add_socket(ret);
	}
	return ret;
}

int socket(int domain, int type, int protocol)
{
	int ret;
	if(init()) return -1;
	DEBUG(1,"socket(%d,%d,%d)",domain,type,protocol);
	if(domain!=PF_INET)
	{
		return fn.socket(domain,type,protocol);
	}
	else
	{
		ret = fn.socket(PF_INET6,type,protocol);
		if(ret>0)
		{
			/* We switch IPV6_V6ONLY off, so ipv4 mapped ipv6 addresses work */
			int on = 0;
			fn.setsockopt(ret,IPPROTO_IPV6, IPV6_V6ONLY, (char*)&on, sizeof(on));

			/* item = */add_socket(ret);
		}
		DEBUG(1," --> %d",ret);	
	}
	return ret;
}

int connect(int socket, const struct sockaddr *address, socklen_t address_len)
{
	struct item_t *item;
	struct sockaddr_in6 sin6;

	if(init()) return -1;
	DEBUG(1,"connect(%d,%s,%d)",socket,ss(address,address_len),address_len);

	item=lookup_socket(socket);
	if(address->sa_family==AF_INET && item)
	{
		struct sockaddr_in *sin = (struct sockaddr_in *)address;
		struct in6_addr *ad6 = lookup_address(&sin->sin_addr);
		memset(&sin6,0,sizeof(sin6));
		sin6.sin6_family=AF_INET6;
		sin6.sin6_port=sin->sin_port;
		address = (const struct sockaddr *)&sin6;
		address_len = sizeof(sin6);
		if(ad6)
		{
			memcpy(sin6.sin6_addr.s6_addr,ad6->s6_addr,16);
		}
		else
		{
			if(sin->sin_addr.s_addr==INADDR_ANY)
			{
				memset(sin6.sin6_addr.s6_addr,0,16);
			}
			else if(sin->sin_addr.s_addr==htonl(INADDR_LOOPBACK)) 
			{
				memset(sin6.sin6_addr.s6_addr,0,16);
				sin6.sin6_addr.s6_addr[15]=1;
			}
			else
			{
				memcpy(&sin6.sin6_addr.s6_addr[12],&sin->sin_addr.s_addr,4);
				memset(&sin6.sin6_addr.s6_addr[10],0xff,2);
			}
		}

		DEBUG(1,"convert %s", ss((struct sockaddr *)sin,sizeof(struct sockaddr_in)));
		DEBUG(1,"     -> %s", ss((struct sockaddr *)&sin6,sizeof(struct sockaddr_in6)));

	}

	return fn.connect(socket,address,address_len);
}

int accept(int socket, struct sockaddr *address, socklen_t *address_len)
{
	struct item_t *item;
	struct sockaddr_storage sa;
	int sock;

	if(init()) return -1;
	DEBUG(1,"accept(%d)",socket);
	sock = fn.accept(socket,(struct sockaddr *)&sa,address_len);
	if(sock>0)
	{
		item = lookup_socket(socket);
		if(sa.ss_family == AF_INET6 && item)
		{
			struct sockaddr_in *sin = (struct sockaddr_in *)address;
			struct sockaddr_in6 *sin6 = (struct sockaddr_in6 *)&sa;
			static const char v4_addr[12] = { 0,0,0,0,0,0,0,0,0,0,0xff,0xff };

			*address_len = sizeof(struct sockaddr_in);
			memset(sin,0,sizeof(struct sockaddr_in));
			sin->sin_family = AF_INET;
			sin->sin_port = sin6->sin6_port;
		
			if(!memcmp(sin6->sin6_addr.s6_addr,v4_addr,12))
			{
				/* ipv4 mapped ipv6 */
				memcpy(&sin->sin_addr.s_addr, &sin6->sin6_addr.s6_addr[12], 4);
			}
			else
			{
				/* Normal ipv6 */
				sin->sin_addr = proxy_address(&sin6->sin6_addr);
			}
			DEBUG(1,"accept mapped %s",ss((struct sockaddr *)sin6, sizeof(struct sockaddr_in6)));
			DEBUG(1,"          --> %s",ss((struct sockaddr *)sin, sizeof(struct sockaddr_in)));

			/* item = */add_socket(sock);
		}
		else
			memcpy(address,&sa,*address_len);
	}
	return sock;
}

int getpeername(int socket, struct sockaddr *address, socklen_t *address_len)
{
	struct item_t *item;
	struct sockaddr_storage sa;
	socklen_t salen;
	int sock;

	if(init()) return -1;
	DEBUG(1,"getpeername(%d,%d)",socket,*address_len);
	salen = sizeof(sa);
	sock = fn.getpeername(socket,(struct sockaddr *)&sa,&salen);
        if(sock>0)
        {
		item = lookup_socket(socket);
                if(sa.ss_family == AF_INET6 && item)
                {
                        struct sockaddr_in *sin = (struct sockaddr_in *)address;
                        struct sockaddr_in6 *sin6 = (struct sockaddr_in6 *)&sa;
                        static const char v4_addr[12] = { 0,0,0,0,0,0,0,0,0,0,0xff,0xff };
	
			/* We don't touch the sin_zero bits so can truncate to
			   sizeof sockaddr_in - 8.  Anything smaller than that
			   isn't going to work */	
			if(*address_len < sizeof(struct sockaddr_in)-8)
			{
				*address_len = sizeof(struct sockaddr_in);
				return -1; 
			}

			
                        *address_len = min(sizeof(struct sockaddr_in),*address_len);
                        memset(sin,0,*address_len);
                        sin->sin_family = AF_INET;
                        sin->sin_port = sin6->sin6_port;

                        if(!memcmp(sin6->sin6_addr.s6_addr,v4_addr,12))
                        {
                                /* ipv4 mapped ipv6 */
                                memcpy(&sin->sin_addr.s_addr, &sin6->sin6_addr.s6_addr[12], 4);
                        }
                        else
                        {
                                /* Normal ipv6 */
                                sin->sin_addr = proxy_address(&sin6->sin6_addr);
                        }
                        DEBUG(1,"getpeername mapped %s",ss((struct sockaddr *)sin6, sizeof(struct sockaddr_in6)));
                        DEBUG(1,"               --> %s",ss((struct sockaddr *)sin, sizeof(struct sockaddr_in)));
                }
                else
		{
			*address_len = min(*address_len, salen);
                        memcpy(address,&sa,*address_len);
		}
        }
	return sock;
}

int bind(int socket, const struct sockaddr *address, socklen_t address_len)
{
	struct item_t *item;
	struct sockaddr_in6 sin6;

	if(init()) return -1;
	DEBUG(1,"bind(%d,%s,%d)",socket,ss(address,address_len),address_len);
      
	item = lookup_socket(socket);
	if(address->sa_family==AF_INET && item)
        {
                struct sockaddr_in *sin = (struct sockaddr_in *)address;
                struct in6_addr *ad6 = lookup_address(&sin->sin_addr);
                memset(&sin6,0,sizeof(sin6));
                sin6.sin6_family=AF_INET6;
                sin6.sin6_port=sin->sin_port;
                address = (const struct sockaddr *)&sin6;
                address_len = sizeof(sin6);
                if(ad6)
                {
                        memcpy(sin6.sin6_addr.s6_addr,ad6->s6_addr,16);
                }
                else
                {
                        if(sin->sin_addr.s_addr==INADDR_ANY)
                        {
                                memset(sin6.sin6_addr.s6_addr,0,16);
                        }
                        else if(sin->sin_addr.s_addr==htonl(INADDR_LOOPBACK))
                        {
                                memset(sin6.sin6_addr.s6_addr,0,16);
                                sin6.sin6_addr.s6_addr[15]=1;
                        }
                        else
                        {
                                memcpy(&sin6.sin6_addr.s6_addr[12],&sin->sin_addr.s_addr,4);
                                memset(&sin6.sin6_addr.s6_addr[10],0xff,2);
                        }
                }

                DEBUG(1,"convert %s", ss((struct sockaddr *)sin,sizeof(struct sockaddr_in)));
                DEBUG(1,"     -> %s", ss((struct sockaddr *)&sin6,sizeof(struct sockaddr_in6)));

        }
	return fn.bind(socket,address,address_len);
}

struct hostent *gethostbyname(const char *name)
{
	struct addrinfo *ai, *p, hint = {0};
	int count;
	char **hp;
	struct in_addr *hpa;

	if(init()) return NULL;
	DEBUG(1,"gethostbyname(%s)",name);

	hint.ai_socktype = SOCK_STREAM; // We don't want duplicae for TCP/UDP

	int ret = getaddrinfo(name, NULL, &hint, &ai);
	if(ret || !ai)
		return NULL;

	for(count=0, p=ai; p; p = p->ai_next)
	{
		if(p->ai_family == AF_INET || p->ai_family==AF_INET6)
			count++;
	}

	if(he.h_addr_list) he.h_addr_list = (char **)realloc(he.h_addr_list,sizeof(char*)*(count+1));
	else he.h_addr_list = (char **)malloc(sizeof(char*)*(count+1));

	if(he_buf) he_buf = (struct in_addr *)realloc(he_buf,sizeof(struct in_addr)*count);
	else he_buf = (struct in_addr *)malloc(sizeof(struct in_addr)*count);

	if(he.h_name) free(he.h_name);
	he.h_name = strdup(name);
	he.h_aliases = NULL;
	he.h_addrtype = AF_INET;
	he.h_length = 4;

	hp = he.h_addr_list;
	hpa = he_buf;

	for(p=ai; p; p=p->ai_next)
	{	
		DEBUG(1,"entry: %d %s", p->ai_family, ss(p->ai_addr,p->ai_addrlen));
		if(p->ai_family==AF_INET)
		{
			struct sockaddr_in *sin = (struct sockaddr_in *)p->ai_addr;
			*hpa = sin->sin_addr;
			*(hp++)=(char*)hpa++;
		}
		if(p->ai_family==AF_INET6)
		{
			struct sockaddr_in6 *sin = (struct sockaddr_in6 *)p->ai_addr;
			*hpa = proxy_address(&sin->sin6_addr);
			*(hp++)=(char*)hpa++;
		}	
	}

	*hp=NULL;

	freeaddrinfo(ai);
	dump_hostent(&he);
	return &he;
}

struct hostent *gethostbyname2(const char *name, int af)
{
	struct hostent *h;

	if(af==AF_INET) return gethostbyname(name);

	if(init()) return NULL;
	DEBUG(1,"gethostbyname(%s,%d)",name,af);
	h=fn.gethostbyname2(name,af);
	dump_hostent(h);
	return h;
}

struct hostent *gethostbyaddr(const void *addr, socklen_t len, int type)
{
	int ret;

	if(init()) return NULL;
	DEBUG(1,"gethostbyaddr(%p,%d,%d)",addr,len,type);

	if(type!=AF_INET)
	{
		struct hostent *h;

		h=fn.gethostbyaddr(addr,len,type);
		dump_hostent(h);
		return h;
	}
	else
	{
		char host[NI_MAXHOST];
		struct sockaddr_in sin = {0};
		sin.sin_family = AF_INET;
		sin.sin_addr.s_addr = ((struct in_addr*)addr)->s_addr;
		ret = getnameinfo((const struct sockaddr *)&sin,sizeof(sin),host,sizeof(host),NULL,0,0);
		if(ret)
			return NULL;
		/* Cheat, by looping back through gethostbyname. */
		return gethostbyname(host);
	}
}

int getsockopt(int socket, int level, int option_name, void *option_value, socklen_t *option_len)
{
	struct item_t *item;
	int ret;
	if(init()) return -1;
	DEBUG(1,"getsockopt(%d,%d,%d,%p,%p)",socket,level,option_name,option_value,option_len);

	item = lookup_socket(socket);	
	if(level == IPPROTO_IP && socket) level = IPPROTO_IPV6;
	ret=fn.getsockopt(socket,level,option_name,option_value,option_len);
	DEBUG(1,"  --> %d",ret);
	return ret;
}

int setsockopt(int socket, int level, int option_name, const void *option_value, socklen_t option_len)
{
	struct item_t *item;
	int ret;
	if(init()) return -1;
	DEBUG(1,"setsockopt(%d,%d,%d,%p,%d)",socket,level,option_name,option_value,option_len);
	
	item = lookup_socket(socket);	
	if(level == IPPROTO_IP && item) level = IPPROTO_IPV6;
	ret=fn.setsockopt(socket,level,option_name,option_value,option_len);
	DEBUG(1,"  --> %d",ret);
	return ret;
}

ssize_t sendto(int socket, const void *buf, size_t len, int flags,
                      const struct sockaddr *address, socklen_t address_len)
{
	struct item_t *item;
        struct sockaddr_in6 sin6;

	if(init()) return -1;

	DEBUG(1,"sendto(%d,%p,%d,%d,%s,%d)",socket,buf,len,flags,ss(address,address_len),address_len);

	item = lookup_socket(socket);	
        if(address->sa_family==AF_INET && item)
        {
                struct sockaddr_in *sin = (struct sockaddr_in *)address;
                struct in6_addr *ad6 = lookup_address(&sin->sin_addr);
                memset(&sin6,0,sizeof(sin6));
                sin6.sin6_family=AF_INET6;
                sin6.sin6_port=sin->sin_port;
                address = (const struct sockaddr *)&sin6;
                address_len = sizeof(sin6);
                if(ad6)
                {
                        memcpy(sin6.sin6_addr.s6_addr,ad6->s6_addr,16);
                }
                else
                {
                        if(sin->sin_addr.s_addr==INADDR_ANY)
                        {
                                memset(sin6.sin6_addr.s6_addr,0,16);
                        }
                        else if(sin->sin_addr.s_addr==htonl(INADDR_LOOPBACK))
                        {
                                memset(sin6.sin6_addr.s6_addr,0,16);
                                sin6.sin6_addr.s6_addr[15]=1;
                        }
                        else
                        {
                                memcpy(&sin6.sin6_addr.s6_addr[12],&sin->sin_addr.s_addr,4);
                                memset(&sin6.sin6_addr.s6_addr[10],0xff,2);
                        }
                }

                DEBUG(1,"convert %s", ss((struct sockaddr *)sin,sizeof(struct sockaddr_in)));
                DEBUG(1,"     -> %s", ss((struct sockaddr *)&sin6,sizeof(struct sockaddr_in6)));

		if(utils_write(socket,sin,&sin6,&buf,&len))
		{
			DEBUG(1,"utils_write failed");
			return -1;
		}
        }

	return fn.sendto(socket,buf,len,flags,address,address_len);
}

ssize_t recvfrom(int socket, void *buf, size_t len, int flags,
                        struct sockaddr *address, socklen_t *address_len)
{
	struct item_t *item;
	ssize_t ret;
        struct sockaddr_storage sa;
        socklen_t salen;
	
	if(init()) return -1;

	DEBUG(1,"recvfrom(%d,%p,%d,%d,%p,%d)",socket,buf,len,flags,address,*address_len);

        salen = sizeof(sa);
	ret = fn.recvfrom(socket,buf,len,flags,(struct sockaddr *)&sa,&salen);

        if(ret!=(ssize_t)-1)
        {
		item = lookup_socket(socket);
                if(sa.ss_family == AF_INET6 && item)
                {
                        struct sockaddr_in *sin = (struct sockaddr_in *)address;
                        struct sockaddr_in6 *sin6 = (struct sockaddr_in6 *)&sa;
                        static const char v4_addr[12] = { 0,0,0,0,0,0,0,0,0,0,0xff,0xff };

                        /* We don't touch the sin_zero bits so can truncate to
                           sizeof sockaddr_in - 8.  Anything smaller than that
                           isn't going to work */
                        if(*address_len < sizeof(struct sockaddr_in)-8)
			{
				*address_len = sizeof(struct sockaddr_in);
                                return -1;
			}

                        *address_len = min(sizeof(struct sockaddr_in),*address_len);
                        memset(sin,0,*address_len);
                        sin->sin_family = AF_INET;
                        sin->sin_port = sin6->sin6_port;

                        if(!memcmp(sin6->sin6_addr.s6_addr,v4_addr,12))
                        {
                                /* ipv4 mapped ipv6 */
                                memcpy(&sin->sin_addr.s_addr, &sin6->sin6_addr.s6_addr[12], 4);
                        }
                        else
                        {
                                /* Normal ipv6 */
                                sin->sin_addr = proxy_address(&sin6->sin6_addr);
                        }
                        DEBUG(1,"getpeername mapped %s",ss((struct sockaddr *)sin6, sizeof(struct sockaddr_in6)));
                        DEBUG(1,"               --> %s",ss((struct sockaddr *)sin, sizeof(struct sockaddr_in)));
		
			if(utils_read(socket,sin,sin6,&buf,&len))
			{
				DEBUG(1,"utils_write failed");
				return -1;
			}
                }
                else
                {
                        *address_len = min(*address_len, salen);
                        memcpy(address,&sa,*address_len);
                }
        }
        return ret;

}

ssize_t recvmsg(int sockfd, struct msghdr *msg, int flags)
{
	if(init()) return -1;

	DEBUG(1,"recvmsg(%d,%p,%d)",sockfd,msg,flags);

	return fn.recvmsg(sockfd,msg,flags);
}

