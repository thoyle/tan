#include <sys/types.h>
#include <sys/socket.h>
#include <sys/mman.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <syslog.h>
#include <stdarg.h>
#include <time.h>
#include <dlfcn.h>

#include "utils.h"

#ifndef LD_PRELOAD
#define LD_PRELOAD "LD_PRELOAD"
#endif

#define API_VER 0x100

#define MAX_HOSTS 1000
struct __global_shared
{
	struct __proxy_host
	{
		time_t stamp;
		struct in6_addr addr;
	}  proxy_hosts[MAX_HOSTS];
	int nexthost;
	int (*alg_init)(int apiver, void* unused);
	int (*alg_read)(int socket, struct sockaddr_in *v4addr, struct sockaddr_in6 *v6addr, void **data, size_t *len);
	int (*alg_write)(int socket, struct sockaddr_in *v4addr, struct sockaddr_in6 *v6addr, const void **data, size_t *len);
};

static struct __global_shared *g;

int debug_level;
void *alg_handle;

void utils_init(void)
{
	char *e;

	g=(struct __global_shared *)mmap(NULL, sizeof(struct __global_shared), PROT_READ|PROT_WRITE, MAP_ANON|MAP_SHARED, -1, 0);
	memset(g,0,sizeof(struct __global_shared));

	e=getenv("__TAN_DEBUG__");	
	if(e) debug_level=atoi(e);
	else debug_level=0;
	e=getenv("__TAN_NOCHILD__");	
	if(e && atoi(e))
	{
		unsetenv(LD_PRELOAD);
	}
	
	if(debug_level)
	{
		openlog("tan", LOG_PID, LOG_USER);
		DEBUG(1,"Debug level set to %d", debug_level);
	}

	e=getenv("__TAN_ALG__");
	if(e)
	{
		alg_handle = dlopen(e,RTLD_NOW);
		if(!alg_handle)
			DEBUG(1,"Unable to load %s",e);
		else
		{
			g->alg_init = (int (*)(int,void*)) dlsym(alg_handle,"alg_init");
			g->alg_read = (int (*)(int, struct sockaddr_in *, struct sockaddr_in6 *, void **, size_t *)) dlsym(alg_handle,"alg_read");
			g->alg_write = (int (*)(int, struct sockaddr_in *, struct sockaddr_in6 *, const void **, size_t *)) dlsym(alg_handle,"alg_write");
		
			if(!g->alg_init && !g->alg_read && !g->alg_write)
			{
				DEBUG(1,"Unable to load %s",e);
				dlclose(alg_handle);
				alg_handle = NULL;
			}

			if(g->alg_init(API_VER, NULL))
			{
				DEBUG(1,"Initialisation of %s failed",e);
				dlclose(alg_handle);
				alg_handle = NULL;
			}
		}
	}
	
	g->nexthost=1;
}

void debug(int lvl, const char *fmt, ...)
{
	if(debug_level >= lvl)
	{
		va_list ap;
		va_start(ap,fmt);
		vsyslog(LOG_INFO, fmt, ap);
	}
}

const char *ss(const struct sockaddr *sa, socklen_t len)
{
	static char host[NI_MAXHOST];
	static char port[NI_MAXSERV];
	if(getnameinfo(sa, len, host, sizeof(host), port, sizeof(port), NI_NUMERICHOST|NI_NUMERICSERV))
		strcpy(host,"???");
	else
	{
		strcat(host,":");
		strcat(host,port);
	}
	return host;
}

void dump_hostent(struct hostent *h)
{
	char **p;
	char host[NI_MAXHOST];

	for(p=h->h_addr_list; *p; p++)
	{
		DEBUG(1,"hostent: %s",inet_ntop(h->h_addrtype, *p, host, sizeof(host)));
	}
}

static int lookup_host(const struct in6_addr *addr)
{
	int n;

	for(n=1; n<g->nexthost; n++)
	{
		if(!memcmp(g->proxy_hosts[n].addr.s6_addr,addr->s6_addr,16))
		{
			time(&g->proxy_hosts[n].stamp);
			return n;
		}
	}
	return 0;
}

struct in6_addr *lookup_address(const struct in_addr *addr)
{
	unsigned long ad = ntohl(addr->s_addr);
	
	if((ad&0xFFFF0000)!=((10<<24)+(255<<16)))
		return NULL;

	ad&=0x0000FFFF;
	if(ad>MAX_HOSTS) return NULL;

	time(&g->proxy_hosts[ad].stamp);
	return &g->proxy_hosts[ad].addr;
}

struct in_addr proxy_address(const struct in6_addr *addr)
{
	struct in_addr newaddr;
	
	int hostid = lookup_host(addr);

	if(!hostid)
	{
		hostid = g->nexthost++;
		memcpy(g->proxy_hosts[hostid].addr.s6_addr,addr->s6_addr,16);
		time(&g->proxy_hosts[hostid].stamp);
	}

	newaddr.s_addr=htonl((10<<24)+(255<<16)+hostid);
	return newaddr;	
}

int utils_read(int socket, struct sockaddr_in *v4addr, struct sockaddr_in6 *v6addr, void **data, size_t *len)
{
	if(g->alg_read) return g->alg_read(socket,v4addr,v6addr,data,len);
	else return 0;
}

int utils_write(int socket, struct sockaddr_in *v4addr, struct sockaddr_in6 *v6addr, const void **data, size_t *len)
{
	if(g->alg_write) return g->alg_write(socket,v4addr,v6addr,data,len);
	else return 0;
}


