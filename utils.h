#ifndef UTILS__H
#define UTILS__H

#define DEBUG(lvl,fmt, ...) debug(lvl, fmt, ## __VA_ARGS__)

void utils_init(void);
void debug(int lvl, const char *fmt, ...);

const char *ss(const struct sockaddr *sa, socklen_t len);
void dump_hostent(struct hostent *h);

struct in6_addr *lookup_address(const struct in_addr *addr);
struct in_addr proxy_address(const struct in6_addr *addr);

int utils_read(int socket, struct sockaddr_in *v4addr, struct sockaddr_in6 *v6addr, void **data, size_t *len);
int utils_write(int socket, struct sockaddr_in *v4addr, struct sockaddr_in6 *v6addr, const void **data, size_t *len);


#endif
